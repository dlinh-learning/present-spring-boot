# #1/bin/bash

# curl -u ${env.user}:${password} 'http://192.168.20.20:8081/repository/SNAPSHOT-petclinic/org/springframework/boot/spring-boot-starter-parent/${Version}/${ArtifactId}-${Version}.jar'

# # 'http://192.168.20.20:8081/repository/SNAPSHOT-petclinic/org/springframework/boot/spring-boot-starter-parent/3.1.0/spring-boot-starter-parent-3.1.0.jar'


#!/bin/bash
sudo apt update -y
sudo  apt install nginx -y
cd /etc/nginx/sites-available
touch spring.conf

sudo ln -s /etc/nginx/sites-available/spring.conf /etc/nginx/sites-enabled/spring.conf

# nohup  java -jar /code/demo.jar & > /home/spring/log.txt 2>&1 &

sudo nginx -t

sudo nginx -s reload

[Unit]
Description=Spring Boot Petclinic
After=syslog.target
After=network.target[Service]
User=root
Type=simple

[Service]
ExecStart=/usr/bin/java -jar /opt/demo.jar
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=Petclinic

[Install]
WantedBy=multi-user.target