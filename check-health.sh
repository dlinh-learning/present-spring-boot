#!/bin/bash

declare -a resource_urls=(
    "http://192.168.20.30:8080"
    "http://192.168.20.40:8080"
    "http://192.168.20.30:8081"
)

for url in "${resource_urls[@]}"; do
    status=$(curl -s "$url" | jq -r '.status')
    
    if [[ "$status" == "healthy" ]]; then
        echo "Resource at $url is healthy"
    else
        echo "Resource at $url is not healthy (Status: $status)"
    fi
done